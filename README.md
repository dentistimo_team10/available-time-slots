# Availability Checker

## Purpose of the component
This component is in charge of calculating & publishing the available time-slots for the requested dental clinic.

## Communication with other components
- Subscribes to [ClientUI](https://git.chalmers.se/courses/dit355/test-teams-formation/team-10/clientui) request messages through MQTT
- Publishes to [ClientUI](https://git.chalmers.se/courses/dit355/test-teams-formation/team-10/clientui) through MQTT

## Working logic
[ClientUI](https://git.chalmers.se/courses/dit355/test-teams-formation/team-10/clientui) sends request for available time-slots for a clinic by sending user id, clinic id, date, through MQTT.

Availability checker is connected to both dental clinic and appointmnet databases. It fetches the data from these databases.

Availability checker calculates the available time-slots and sends it to [ClientUI](https://git.chalmers.se/courses/dit355/test-teams-formation/team-10/clientui), through MQTT.

## Link to main documentation repository
[Documentation Repository](https://git.chalmers.se/courses/dit355/test-teams-formation/team-10/documentation)

## Component setup
1. Clone the repository
```
https://git.chalmers.se/courses/dit355/test-teams-formation/team-10/availabilitychecker.git
```
2. We use an online broker, as shown in `mqtt/connect.js`, if you would like to change this to a local config then:
- Install MQTT-broker
- Download [Mosquitto](https://mosquitto.org/)
- Update `mqtt/connect.js` with your local settings
3. Ensure you have installed [Node.js](https://nodejs.org/en/)
4. Navigate to the server folder in the repository
5. Open the folder in a terminal
6. Install all dependencies with
```
npm install
```
7. Run availability checker with
```
npm run dev
```
8. Availibiltiy checker should now be running.
