var clinicModel = require('../Models/Clinics');
var appointmentModel = require('../Models/Appointments');

// clinics from database
exports.getClinicDocs = async function () {
    const document = await clinicModel.find({})
    const dentists = document[0].dentists
    const dentistsObj = {
        dentists: dentists
    }
    const str = JSON.stringify(dentistsObj);
    const json = JSON.parse(str);
    //return str;
    return json;
}

// appointments from database
exports.getAppointmentDocs = async function () {
    return new Promise((resolve, reject) => {
        appointmentModel.find()
        .then(data => resolve({ msg: data }))
        .catch(err =>  reject({err: err }))
    })
}