const mqtt = require('mqtt')
const { v4: uuidv4 } = require('uuid')
const options = {
  host: 'broker.emqx.io',
  port: 8083,
  protocol: 'ws',
  endpoint: '/mqtt',
  clean: true,
  connectTimeout: 4000,
  reconnectPeriod: 4000,
  clientId: uuidv4(),
  username: 'team-10',
  password: 'team-10'
}

const connectUrl = `${options.protocol}://${options.host}:${options.port}${options.endpoint}`
const mqttClient = mqtt.connect(connectUrl, options);
// export connection to mqtt
module.exports = mqttClient
