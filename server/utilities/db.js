const express = require("express");
const mongoose = require("mongoose");
//const mongoURI = process.env.MONGODB_URI || 'mongodb://localhost:27017/dentistDB';
const mongoURI = process.env.MONGODB_URI || 'mongodb+srv://team10:team10@cluster0.3l2bt.mongodb.net/myFirstDatabase?retryWrites=true&w=majority';
const port = process.env.PORT || 3001;

function connect() {
    mongoose.connect(mongoURI, { useNewUrlParser: true, useUnifiedTopology: true }, function (err) {
        if (err) {
            console.error(`Failed to connect to MongoDB with URI: ${mongoURI}`);
            console.error(err.stack);
            process.exit(1);
        }
        console.log(`AvailabilityChecker connected to MongoDB with URI: ${mongoURI}`);
    });
}

var app = express();
app.use(express.json());
app.listen(port, function (err) {
    if (err) throw err;
    console.log(`AvailabilityChecker: http://localhost:${port}/api/`);
});

module.exports = { connect: connect() }