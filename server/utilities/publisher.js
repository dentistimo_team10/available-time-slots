const mqttClient = require("./connect");
const topics = require('./topics')
const publishTopics = topics.PUBLISH_TOPICS;



// Class used to publish
class Publisher {
  constructor() {}

  publishData(msg, userId) {
    var uniqueTopic = publishTopics.AVAILABILITY_PUBLISH_TOPIC + '/' + userId;
    mqttClient.publish(uniqueTopic, msg, {qos:1});
    console.log(uniqueTopic);
  }
}

module.exports = Publisher;
