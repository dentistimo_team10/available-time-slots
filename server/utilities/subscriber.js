const mqttClient = require('./connect')

// Class used to create subscriptions 

class Subscriber {
    constructor() {
    }

    subscribeToTopic(topic) {
        mqttClient.subscribe(topic, { qos: 1 })
    }

}

module.exports = Subscriber