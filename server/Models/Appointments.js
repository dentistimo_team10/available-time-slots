const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const appointmentSchema = new Schema({
	dentistID: { type: Number, required: true },
	userId: { type: String, required: true },
    issuance: { type: String, required: true, unique: true },
	requestId: { type: Number, required: true },
	selectedTime: [{ type: String, required: true }],
	selectedDate: [{ type: String, required: true}],
});


module.exports = mongoose.model("appointments", appointmentSchema);